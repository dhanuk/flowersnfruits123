<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if(!function_exists('my_file'))
{
	function my_file($filename,$type)
	{
		$path=base_url()."public/";
		
		if($type==1)
		{
			$str= "<link href='".$path."css/$filename.css' rel='stylesheet'/>";
		}
		if($type==2)
		{
			$str="<script src='".$path."js/$filename.js' ></script>";
		}
		
		return $str;
	}
}
if(!function_exists('my_file1'))
{
	function my_file1($filename,$type)
	{
		$path=base_url()."public/";
		
		if($type==1)
		{
			$str= "<link href='".$path."css/$filename.css' rel='stylesheet'/>";
		}
		if($type==2)
		{
			$str="<script src='".$path."js/$filename.js' ></script>";
		}
		
		return $str;
	}
}
if(!function_exists('my_files_in_vendor'))
{
	function my_files_in_vendor($filename,$type)
	{
		$path=base_url()."public/vendors/";
		
		if($type==1)
		{
			$str= "<link href='".$path."$filename.css' rel='stylesheet'/>";
		}
		if($type==2)
		{
			$str="<script src='".$path."$filename.js' ></script>";
		}
		
		return $str;
	}
}



if(!function_exists('get_doc'))
{

	function get_doc($uid,$nm)
	{
		
			$CI =& get_instance();
			$CI->db->select('LCMU_FILENAME ');
			$CI->db->where('LCMU_CASEID',$uid);
			$CI->db->where('LCMU_TITLE',$nm);	
			$query=$CI->db->get('lcm_uploads');
			return $query->result();//return $query->row_array();
		
	}
}




if(!function_exists('get_client'))
{

	function get_client($uid)
	{
		
			$CI =& get_instance();
			$CI->db->select('LCMC_FIRST,LCMC_MIDDLE,LCMC_SURNAME,LCMC_NAME_OF_ORG');
			$CI->db->where('LCMC_ID',$uid);	
			$query=$CI->db->get('lcm_client');
			return $query->row_array();
		
	}
}

if(!function_exists('get_lov'))
{
	function get_lov($uid)
	{
		
			$CI =& get_instance();
			$CI->db->select('LCML_VALUE');
			$CI->db->where('LCML_ID',$uid);	
			$query=$CI->db->get('lcm_lov');
			return $query->row_array();
		
	}
}

if(!function_exists('dropdown'))
{
	function dropdown($field1,$field2,$table,$condition,$name)
	{
			$CI =& get_instance();
			
			echo "<select name='$name' class='form-control' id='$name'>";
			
			$CI->db->select("$field1,$field2"); 
			$CI->db->from($table);
			$CI->db->where($condition);
			$ans = $CI->db->get();
			
			$ans1 = $ans->result();
			
			if($ans->num_rows() == 0)
			{
				echo"<option>No data</option>";
			}
			else
			{
				
				foreach($ans1 as $key=>$value)
				{	
					echo "<option value='".$value->$field1."'>".$value->$field2."</option>";
				}
			}
			echo "</select>";
	}
}

if(!function_exists('dropdown_with_select'))
{
	function dropdown_with_select($field1,$field2,$id,$table,$condition,$name)
	{
		
		
			$CI =& get_instance();
			echo "<select name='$name' class='form-control' id='$name'>";
			
			$CI->db->select("$field1,$field2"); 
			$CI->db->from($table);
			if($condition != '')
			{
				$CI->db->where($condition);
			}	
			
			$ans = $CI->db->get();
			
			$ans1 = $ans->result();
			//echo $CI->db->last_query();
			
			if($ans->num_rows() == 0)
			{
				echo"<option>No data</option>";
			}
			else
			{
				
				foreach($ans1 as $key=>$value)
				{	
					if($id == $value->$field1)
					echo "<option value='".$value->$field1."' selected/>".$value->$field2."</option>";
					else
					echo "<option value='".$value->$field1."'/>".$value->$field2."</option>";
				}
			}
			echo "</select>";
	}
}

if(!function_exists('dropdown_with_select_multiple'))
{
	function dropdown_with_select_multiple($field1,$field2,$id,$table,$condition,$name)
	{
		
		
			$CI =& get_instance();
			echo "<select name='billgencol[]' class='form-control' id='$name' multiple='multiple'>";
			
			$CI->db->select("$field1,$field2"); 
			$CI->db->from($table);
			$CI->db->where($condition);
			$ans = $CI->db->get();
			
			$ans1 = $ans->result();
			
			if($ans->num_rows() == 0)
			{
				echo"<option>No data</option>";
			}
			else
			{
				
				foreach($ans1 as $key=>$value)
				{	
					if($id == $value->$field1)
					echo "<option value='".$value->$field1."' selected/>".$value->$field2."</option>";
					else
					echo "<option value='".$value->$field1."'/>".$value->$field2."</option>";
				}
			}
			echo "</select>";
	}
}



if(!function_exists('select_image'))
{
	function select_image($field1,$field2)
	{
		$CI = get_instance();
		$result=$CI->myclass->select("propimg_path,subcat_id,subcat_name","dv_property,dv_propimage,dv_cat,dv_subcat","cat_id='$field1' and subcat_name='$field2' and cat_id=subcat_catid and subcat_id=prop_subcatid and prop_id=propimg_propid limit 1");
		return $result;
	}
}

if(!function_exists('select_id'))
{
	function select_id($field1)
	{
		$CI=get_instance();
		$result1=$CI->myclass->select("cat_id","dv_cat","cat_name='$field1'");
		return $result1[0]->cat_id;
	}
}

if(!function_exists('getcity'))
{
	function getcity($id)
	{
		$CI=get_instance();
		$result2=$CI->myclass->select("city_name","dv_city","city_id='$id'");
		return $result2[0]->city_name;
	}
}
function encode_url($string, $key="", $url_safe=TRUE)
{
    if($key==null || $key=="")
    {
        $key="tyz_mydefaulturlencryption";
    }
      $CI =& get_instance();
    $ret = $CI->encrypt->encode($string, $key);

    if ($url_safe)
    {
        $ret = strtr(
                $ret,
                array(
                    '+' => '.',
                    '=' => '-',
                    '/' => '~'
                )
            );
    }

    return $ret;
}
function decode_url($string, $key="")
{
     if($key==null || $key=="")
    {
        $key="tyz_mydefaulturlencryption";
    }
        $CI =& get_instance();
    $string = strtr(
            $string,
            array(
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        );

    return $CI->encrypt->decode($string, $key);
}
if(!function_exists('p_mail'))
{
	function p_mail($encode,$email)
	{
		
			//print_r("<a href=".site_url('index.php/register/reset_password?abc='.$encode).">Link</a>");
			$to = $email;
			$subject = "Reset Your Password";
			$from ="info@creaadesigns.com";
			$str=base_url()."index.php/register/verify_pass"; 
			$url_to_be_send="<a href=".site_url('index.php/register/reset_password?abc='.$encode).">Link</a>";
			$message = "
			<html>
			<head>
			<title>HTML email</title>
			</head>
				<body>
				link:Click on the link to reset your password".$url_to_be_send."
				</body>
			</html>
				";
						
			// Always set content-type when sending HTML email
		$headers  = "MIME-Version: 1.0\r\n";
	    $headers .= "Content-type: text/html; charset=UTF-8\r\n";
	    $headers .= "From: <".$from. ">" ;
						
			if(mail($to,$subject,$message,$headers))
			{
				echo "1";			
			} 
			else
			{
				echo "Your Enquiry is not send";
			}
			
	}
}
if(!function_exists('get_company'))
{
	function get_company($uid)
	{
		$CI=get_instance();
		$result2=$CI->myclass->select("log_comp","c_login","log_id='$uid'");
		//print_r($result2); 
		$comp_id=$result2[0]->log_comp;
		//print_r($comp_id);
		$comp=$CI->myclass->select("comp_name,comp_id","c_company","comp_id='$comp_id'");
		//print_r($comp);
		if(is_array($comp) && !empty($comp))
		{
			return $comp;
		}
	}
}	
if(!function_exists('get_mobile'))
{
	function get_mobile($uid)
	{
		$CI=get_instance();
		$comp=$CI->myclass->select("log_contno","c_login","log_id='$uid'");
		$cont_no=$comp[0]->log_contno;
		return $cont_no;
	}
}
if(!function_exists('get_port_name'))
{
	function get_port_name($cnt_port)
	{
		$CI=get_instance();
		$comp1=$CI->myclass->select("port_name","c_port","port_id='$cnt_port'");
		$cont_name1=$comp1[0]->port_name;
		return $cont_name1;
	}
}
if(!function_exists(''))
{
	function get_ship_name($ship_id)
	{
		$CI=get_instance();
		$comp1=$CI->myclass->select("shipping_name","c_shipping","shipping_id='$ship_id'");
		if(is_array($comp1))
		{
		$cont_name1=$comp1[0]->shipping_name;
		return $cont_name1;
		}
	}
}
?>